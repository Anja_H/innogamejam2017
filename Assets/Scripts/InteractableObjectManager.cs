﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InteractableObjectManager : MonoBehaviour 
{
	public static InteractableObjectManager instance;

	List<ChangeEffect> pickups = new List<ChangeEffect> ();

	void Awake()
	{
		if (instance == null)
			instance = this;
		else
			Destroy (this);
	}

	public void Register(ChangeEffect effect)
	{
		if (instance.pickups.Contains(effect) == false)
			instance.pickups.Add (effect);
	}

	public void ResetAllInteractables()
	{
		foreach (var effect in instance.pickups) 
		{
			// Moving interactables
			MoveOnDisplay moveOnDisplay = effect.gameObject.GetComponent<MoveOnDisplay>();

			if (moveOnDisplay != null) 
			{
				moveOnDisplay.Reset ();
			}
			// Static interactables
			else if (effect.gameObject.activeInHierarchy == false)
			{
				effect.gameObject.SetActive(true);
			}
		}
	}

	public void ResetFollowingInteractables(Vector3 position)
	{
		foreach (var effect in instance.pickups) 
		{
			// Moving interactables
			MoveOnDisplay moveOnDisplay = effect.gameObject.GetComponent<MoveOnDisplay>();

			if (moveOnDisplay != null) 
			{
				moveOnDisplay.Reset ();
			}
			// Static interactables
			else if (effect.transform.position.x > position.x &&
				effect.gameObject.activeInHierarchy == false)
			{
				effect.gameObject.SetActive(true);
			}
		}
	}
}
