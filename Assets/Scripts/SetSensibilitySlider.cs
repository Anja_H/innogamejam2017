﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SetSensibilitySlider : MonoBehaviour 
{
	void Start () 
	{
		GetComponent<Slider> ().value = Wave.instance.inputMinInDb;
	}
}
