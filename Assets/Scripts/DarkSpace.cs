﻿using UnityEngine;
using System.Collections;

public class DarkSpace : MonoBehaviour 
{
	[SerializeField]
	float goVisibleTime = 0.5f;

	[SerializeField]
	float visibleTime = 0.3f;

	SpriteRenderer renderer2D;

	void OnParticleCollision(GameObject other)
	{
		if (other.tag == "Dark") 
		{
			renderer2D = other.GetComponent<SpriteRenderer> ();
			other.GetComponent<MonoBehaviour>().StartCoroutine (LerpInAndOut(other));
			other.gameObject.tag = "Untagged";
		}
	}

	IEnumerator LerpInAndOut(GameObject myOb)
	{
		StartCoroutine(LerpColor (Color.white));
		yield return new WaitForSeconds (goVisibleTime + visibleTime);
		StartCoroutine(LerpColor (Color.black));
		yield return new WaitForSeconds (goVisibleTime);
		myOb.tag = "Dark";
		 

		yield return null;
	}

	IEnumerator LerpColor(Color endColor)
	{
		float elapsedTime = 0;
		Color startColor = renderer2D.color;

		while (elapsedTime < goVisibleTime) 
		{
			renderer2D.color = Color.Lerp (startColor, endColor, elapsedTime / goVisibleTime);
			elapsedTime += Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}

		yield return null;
	}
}
