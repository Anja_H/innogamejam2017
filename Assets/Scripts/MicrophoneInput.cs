﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class MicrophoneInput : MonoBehaviour 
{
	AudioSource source;

	public static float smoothVolumeDB;

	// Convert samples to db
	int qSamples = 1024;  // array size

	float refValue = 0.1f; // RMS value for 0 dB
	float rmsValue;   // sound level - RMS
	float dbValue;    // sound level - dB
	float[] samples; // audio samples

	// Smooth db over frames
	[SerializeField]
	int smoothingFrameCount = 10;

	int currentSmoothingIndex = 0;
	float[] volumeOverFrames;

	bool recordingStarted = false;

	void Awake()
	{
		source = GetComponent<AudioSource> ();
		samples = new float[qSamples];
		volumeOverFrames = new float[smoothingFrameCount];
	}

	void Start ()
	{
		source.clip = Microphone.Start (null, true, 10, 44100);
		source.loop = true; // Set the AudioClip to loop

		StartCoroutine(WaitForRecording());
	}

	IEnumerator WaitForRecording()
	{
		string firstMicrophone = Microphone.devices [0];

		// Wait until the recording has started
		while ((Microphone.GetPosition (firstMicrophone) > 0) == false) 
		{ 
			yield return new WaitForSecondsRealtime (0.5f);
		} 

		source.Play (); // Play the audio source!
		recordingStarted = true;

		yield return null;
	}

	void Update ()
	{
		if (recordingStarted)
			AddToSmoothing (GetVolume ());
	}

	float GetVolume()
	{
		source.GetOutputData(samples, 0); // fill array with samples
		int i;
		float sum = 0;

		for (i=0; i < qSamples; i++)
		{
			sum += samples[i]*samples[i]; // sum squared samples
		}

		rmsValue = Mathf.Sqrt(sum/qSamples); // rms = square root of average
		dbValue = 20*Mathf.Log10(rmsValue/refValue); // calculate dB

		if (dbValue < -160) 
			dbValue = -160; // clamp it to -160dB min

		return dbValue;
	}

	void AddToSmoothing(float currentVolumeInDb)
	{
		volumeOverFrames [currentSmoothingIndex] = currentVolumeInDb;

		// Calculate next index.
		currentSmoothingIndex++;

		if (currentSmoothingIndex >= smoothingFrameCount)
			currentSmoothingIndex = 0;

		float sum = 0;
		for (int i = 0; i < smoothingFrameCount; i++) 
		{
			sum += volumeOverFrames [i];
		}

		smoothVolumeDB = sum / smoothingFrameCount;
	}
}
