﻿using UnityEngine;
using System;
using System.Collections;

public class SavePoint : MonoBehaviour 
{
	public static Action<SavePoint> SavePointVisited;

	public int savedScore { get; private set;}
	public float savedWaveAmount { get; private set; }

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Player") 
		{
			savedScore = Score.instance.amount;
			savedWaveAmount = Wave.instance.amount;

			if (SavePointVisited != null)
				SavePointVisited.Invoke (this);	
		}
	}
}
