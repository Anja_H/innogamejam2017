﻿using UnityEngine;
using System;
using System.Collections;

public class Wave : MonoBehaviour 
{
	public Action<WaveAmountChangedArgs> AmountChanged;
	public static Wave instance;

	public float amount
	{
		get
		{
			return _amount;
		}
		private set 
		{
			if (value >= 0 && value <= 100) 
			{
				if (AmountChanged != null) 
				{
					_previousAmount = _amount;
					WaveAmountChangedArgs args = new WaveAmountChangedArgs (_previousAmount, value);
					AmountChanged.Invoke (args);
				}
				_amount = value;
			}
		}
	}
	float _amount;
	float _previousAmount;

	[SerializeField]
	float recoverPerSecond = 0.05f;

	[SerializeField, Range(0,100)]
	int startAmount;

	[Header("Use per Microphone"), SerializeField]
	float usePerSecond = 0.1f;
	public float inputMinInDb = -20;
	string minDbName = "Minimum Input in decibel";

	void Awake()
	{
		if (instance == null)
			instance = this;
		else
			Destroy (this);

		amount = startAmount;

		// Get the saved amount if it excists.
		inputMinInDb = PlayerPrefs.GetFloat (minDbName, inputMinInDb);
	}

	void Update()
	{
		// Auto regen
		amount += recoverPerSecond * Time.deltaTime;

		// Use wave
		if (MicrophoneInput.smoothVolumeDB >= inputMinInDb)
			amount -= usePerSecond * Time.deltaTime;
	}

	public void Add(float amount)
	{
		this.amount += amount;
	}

	public void Set(float amount)
	{
		this.amount = amount;
	}

	public void SetMinDb(float value)
	{
		inputMinInDb = value;
	}

	public void SaveCurrentMinDbInPrefs()
	{
		PlayerPrefs.SetFloat (minDbName, inputMinInDb);
	}

	public void Reset()
	{
		amount = startAmount;
	}

	bool IsAmountAvailable(float amount)
	{
		return (this.amount - amount) >= 0;
	}
}

public class WaveAmountChangedArgs
{
	public readonly float previousAmount;
	public readonly float currentAmount;
	public readonly WaveState state;

	public WaveAmountChangedArgs(float previous, float current)
	{
		this.previousAmount = previous;
		this.currentAmount = current;

		if ((current - previous) < 0)
			this.state = WaveState.Decreasing;
		else if ((current - previous) > 0)
			this.state = WaveState.Increasing;
		else
			this.state = WaveState.None;
	}
}

public enum WaveState
{
	None,
	Increasing,
	Decreasing
}
