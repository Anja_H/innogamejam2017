﻿using UnityEngine;
using System.Collections;

public class MoveOnDisplay : MonoBehaviour
{
	public float horizontalSpeed = 1f;
	public Vector3 startPosition { get; private set; }

	bool moving;
	Renderer renderer2D;

	void Awake()
	{
		startPosition = transform.position;
		renderer2D = GetComponent<Renderer> ();
	}

	void OnBecameVisible()
	{
		moving = true;
	}

	void Update()
	{
		if (moving == false)
			return;

		transform.position = new Vector2 (transform.position.x - horizontalSpeed * Time.deltaTime, transform.position.y);
	}

	public void Reset()
	{
		gameObject.SetActive (true);
		transform.position = startPosition;
		moving = renderer2D.isVisible;
	}
}
