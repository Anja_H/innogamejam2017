﻿using UnityEngine;
using System;
using System.Collections;

public class Score : MonoBehaviour 
{
	public static Action<int> OnScoreChanged;

	public static Score instance;

	public int amount 
	{
		get{return _value; }
		private set 
		{
			_value = value; 
			if (OnScoreChanged != null)
				OnScoreChanged.Invoke (value);
		}
	}
	int _value;

	void Awake()
	{
		if (instance == null)
			instance = this;
		else
			Destroy (this);

		amount = 0;
	}

	public void Add(int value)
	{
		this.amount += value;
	}

	public void Set(int value)
	{
		this.amount = value;
	}
}
