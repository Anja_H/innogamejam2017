﻿using UnityEngine;
using System;
using System.Collections;

public class InputManager : MonoBehaviour 
{	
	public static bool touching 
	{
		get;
		private set;
	}

	public static Action TouchBegin;
	public static Action TouchEnd;


	void Update()
	{
		if (Input.GetMouseButtonDown (0)) 
		{
			touching = true;

			if (TouchBegin != null)
				TouchBegin.Invoke ();
		}

		if (Input.GetMouseButtonUp (0)) 
		{
			touching = false;

			if (TouchEnd != null)
				TouchEnd.Invoke ();
		}
	}
}
