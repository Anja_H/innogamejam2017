﻿using UnityEngine;
using System;
using System.Collections;

public class PlayerLife : MonoBehaviour 
{
	[SerializeField]
	int startLifeCount = 3;

	public static Action<LifeCountChangedEventArgs> LifeCountChanged;

	public int lifeCount
	{
		get {return _lifeCount; }
		private set 
		{
			if (LifeCountChanged != null) 
			{
				int previousCount = _lifeCount;
				LifeCountChangedEventArgs args = new LifeCountChangedEventArgs (previousCount, value);
				LifeCountChanged.Invoke (args);
			}

			_lifeCount = value;
		}
	}
	private int _lifeCount;

	void Awake()
	{
		Reset ();
	}

	public void Kill()
	{
		if (enabled)
			lifeCount = 0;
	}

	public void AddLife(int amount)
	{
		if (enabled)
			lifeCount += amount;
	}

	public void Reset()
	{
		enabled = true;
		lifeCount = startLifeCount;
	}
}

public class LifeCountChangedEventArgs
{
	public readonly int previousLifeCount;
	public readonly int currentLifeCount;

	public LifeCountChangedEventArgs(int previous, int current)
	{
		this.previousLifeCount = previous;
		this.currentLifeCount = current;
	}
}
