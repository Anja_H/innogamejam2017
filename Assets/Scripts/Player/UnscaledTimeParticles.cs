﻿using UnityEngine;
using System.Collections;

public class UnscaledTimeParticles : MonoBehaviour 
{
	ParticleSystem particles;

	void Awake()
	{
		particles = GetComponent<ParticleSystem> ();
	}

	void Update()
	{
		if (Time.timeScale < 0.01f)
		{
			particles.Simulate(Time.unscaledDeltaTime, true, false);
		}
		else
			particles.Simulate(Time.deltaTime, true, false);

	}
}
