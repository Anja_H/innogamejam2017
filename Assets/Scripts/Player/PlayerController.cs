﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour 
{
	PlayerMovement movement;
	PlayerLife life;
	Wave wave;

	void Awake()
	{
		movement = GetComponent<PlayerMovement> ();
		life = GetComponent<PlayerLife> ();
		wave = GetComponent<Wave> ();

		PlayerLife.LifeCountChanged += OnLifeCountChanged;
		GameManager.GameOver += OnGameOver;

	}

	void OnDestroy()
	{
		GameManager.GameOver -= OnGameOver;
		PlayerLife.LifeCountChanged -= OnLifeCountChanged;
	}

	void OnLifeCountChanged(LifeCountChangedEventArgs args)
	{
		// If we lost life but are not death
		if (args.currentLifeCount - args.previousLifeCount < 0 && args.currentLifeCount > 0) 
		{
			SavePointManager.instance.ResetToLastSavePoint (transform);
			movement.OnSpawnMovement ();
		}
	}

	void OnGameOver()
	{
		movement.moving = false;
		life.enabled = false;
	}

	public void ResetPlayer()
	{
		movement.Reset ();
		life.Reset ();
		wave.Reset ();
	}
}
