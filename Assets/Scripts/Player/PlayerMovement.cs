﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour 
{
	public bool moving = true;
	public float horizontalSpeed = 1f;
	public float verticalSpeed = 5f;

	private float maxYScreenPosition;

	private Rigidbody2D rigidb;
	private Animator animator;

	void Awake()
	{
		float screenHeightInWorld = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height, 0)).y;
		maxYScreenPosition = screenHeightInWorld - GetComponent<Renderer>().bounds.extents.y;

		rigidb = GetComponent<Rigidbody2D> ();
		animator = GetComponent<Animator> ();

		InputManager.TouchBegin += OnTouchBegin;
		InputManager.TouchEnd += OnTouchEnd;

		Reset ();
	}

	void OnDestroy()
	{
		InputManager.TouchBegin -= OnTouchBegin;
		InputManager.TouchEnd -= OnTouchEnd;
	}
		
	void Update()
	{
		if (moving == false)
			return;

		// Move right
		transform.position = new Vector2 (transform.position.x + horizontalSpeed * Time.deltaTime, transform.position.y);

		// Move up
		if (InputManager.touching)
			transform.position = new Vector2 (transform.position.x, transform.position.y + verticalSpeed * Time.deltaTime);

		// Clamp at screenheight
		if (transform.position.y > maxYScreenPosition)
			transform.position = new Vector3 (transform.position.x, maxYScreenPosition, transform.position.z);
	}

	public void Reset()
	{
		transform.position = Vector3.zero;
		rigidb.velocity = Vector3.zero;
	}

	public void OnSpawnMovement()
	{
		rigidb.velocity = Vector2.zero;
		animator.SetTrigger ("Spawn");
		StartCoroutine (WaitAfterRespawn ());
	}

	IEnumerator WaitAfterRespawn()
	{
		moving = false;
		Time.timeScale = 0;

		yield return new WaitForSecondsRealtime (0.5f);

		Time.timeScale = 1;
		moving = true;

		yield return null;
	}

	// --- Movement up and down ---
	// Disable Physics while touching to prevent heightfighting
	void OnTouchBegin()
	{
		if (moving == false)
			return;
		
		rigidb.velocity = Vector3.zero;
		rigidb.gravityScale = 0.1f;
	}

	public void OnTouchEnd()
	{
		rigidb.gravityScale = 1f;
	}
}
