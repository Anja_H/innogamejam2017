﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour 
{
	[SerializeField]
	GameObject player;

	void LateUpdate()
	{
		transform.position = new Vector3 
			(
				player.transform.position.x, 
				transform.position.y, 
				transform.position.z
			);
	}
}
