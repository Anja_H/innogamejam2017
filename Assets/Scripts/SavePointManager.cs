﻿using UnityEngine;
using System.Collections;

public class SavePointManager : MonoBehaviour 
{
	public static SavePointManager instance;

	[SerializeField]
	InteractableObjectManager pickupManager;

	SavePoint lastSavePoint;

	void Awake()
	{
		if (instance == null)
			instance = this;
		else
			Destroy (this);

		SavePoint.SavePointVisited += OnSavePointVisited;
	}

	void OnSavePointVisited(SavePoint point)
	{
		lastSavePoint = point;
	}

	public void ResetToLastSavePoint(Transform player)
	{
		// Reset player
		player.position = lastSavePoint.transform.position;

		// Reset variables
		Score.instance.Set (lastSavePoint.savedScore);
		Wave.instance.Set (lastSavePoint.savedWaveAmount);

		// Reset level
		pickupManager.ResetFollowingInteractables (lastSavePoint.transform.position);
	}
}
