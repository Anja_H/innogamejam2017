﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MenuManager : MonoBehaviour 
{
	public void LoadGame()
	{
		SceneManager.LoadScene ("Game");
	}

	public void LoadMenu()
	{
		SceneManager.LoadScene ("MainMenu");
	}

	public void LoadMicrConfic()
	{
		SceneManager.LoadScene ("MicroConfig");
	}

	public void QuitGame()
	{
		Application.Quit ();
	}
}
