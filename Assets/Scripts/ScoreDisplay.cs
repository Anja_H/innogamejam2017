﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreDisplay : MonoBehaviour 
{
	Text lable;

	void Awake()
	{
		lable = GetComponent<Text> ();
		Score.OnScoreChanged += OnScoreChanged;
	}

	void OnDestroy()
	{
		Score.OnScoreChanged -= OnScoreChanged;
	}

	void OnScoreChanged(int value)
	{
		lable.text =  value.ToString();
	}
}
