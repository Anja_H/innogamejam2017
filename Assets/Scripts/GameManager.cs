﻿using UnityEngine;
using System;
using System.Collections;

public class GameManager : MonoBehaviour 
{
	public static Action GameOver;

	[SerializeField]
	Canvas replayScreen;

	void Awake()
	{
		PlayerLife.LifeCountChanged += OnLifeCountChanged;
		Time.timeScale = 0;
	}

	void OnDestroy()
	{
		PlayerLife.LifeCountChanged -= OnLifeCountChanged;
	}

	void OnLifeCountChanged(LifeCountChangedEventArgs args)
	{
		if (args.currentLifeCount <= 0) 
		{
			if (GameOver != null)
				GameOver.Invoke ();
			
			StartCoroutine (OpenEndscreen (0.5f));
		}
	}

	IEnumerator OpenEndscreen(float delayInSeconds)
	{
		yield return new WaitForSecondsRealtime (delayInSeconds);

		StopTime ();
		replayScreen.enabled = true;

		yield return null;
	}

	public void StopTime()
	{
		Time.timeScale = 0;
	}

	public void ContinueTime()
	{
		Time.timeScale = 1;
	}
}
