﻿using UnityEngine;
using System.Collections;

public class WaveEffect : MonoBehaviour 
{
	ParticleSystem particleEffect;

	void Awake()
	{
		particleEffect = GetComponent<ParticleSystem> ();
	}

	void Start()
	{
		Wave.instance.AmountChanged += OnWaveAmountChanged;
	}

	void OnDestroy()
	{
		Wave.instance.AmountChanged -= OnWaveAmountChanged;

	}

	void OnWaveAmountChanged( WaveAmountChangedArgs args)
	{
		var em = particleEffect.emission;

		if (args.state == WaveState.Decreasing) 
		{
			if (em.enabled == false) 
			{
				em.enabled = true;
			}
		} 
		else if (em.enabled) 
		{
			em.enabled = false;
		}
	}
}
