﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WaveDisplay : MonoBehaviour 
{
	Slider slider;

	[SerializeField]
	Image microphoneInputFeedback;

	[SerializeField]
	GameObject[] highlights;

	void Awake()
	{
		slider = GetComponent<Slider> ();
	}

	void Start()
	{
		Wave.instance.AmountChanged += OnWaveAmountChanged;
	}

	void Update()
	{
		// Set microphone input feedback
		float fillAmount = Mathf.InverseLerp( Wave.instance.inputMinInDb -20f, Wave.instance.inputMinInDb, MicrophoneInput.smoothVolumeDB);
		microphoneInputFeedback.fillAmount = fillAmount;
	}

	void OnDestroy()
	{
		Wave.instance.AmountChanged -= OnWaveAmountChanged;
	}

	void OnWaveAmountChanged (WaveAmountChangedArgs args)
	{
		slider.value = args.currentAmount;

		// Set heighlight
		if (args.state == WaveState.Decreasing) 
		{
			foreach (var item in highlights)
				item.SetActive (true);
		} 
		// Take an representative gameObject to check the visibility.
		else if (highlights[0].activeInHierarchy == true) 
		{
			foreach (var item in highlights) 
				item.SetActive (false);
		}
	}
}
