﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LifeDisplay : MonoBehaviour 
{
	[SerializeField]
	GameObject lifeDisplayPrefab;
	
	void Awake()
	{
		PlayerLife.LifeCountChanged += OnLifeChanged;
	}

	void OnDestroy()
	{
		PlayerLife.LifeCountChanged -= OnLifeChanged;
	}

	void OnLifeChanged(LifeCountChangedEventArgs args)
	{
		foreach (Transform child in transform) 
		{
			Destroy (child.gameObject);
		}

		for (int i = 0; i < args.currentLifeCount; i++) 
		{
			Instantiate (lifeDisplayPrefab, transform);
		}
	}
}
