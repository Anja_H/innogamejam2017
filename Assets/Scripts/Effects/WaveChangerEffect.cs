﻿using UnityEngine;
using System.Collections;

public class WaveChangerEffect : ChangeEffect 
{
	public override void ApplyEffect (int amount, GameObject other)
	{
		Wave.instance.Add (amount);
	}
}
