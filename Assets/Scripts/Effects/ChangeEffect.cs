﻿using UnityEngine;
using System.Collections;

public class ChangeEffect : MonoBehaviour
{
	public bool disableAfter = true;
	public bool moving = false;

	public int changeAmount = 1;

	void Start()
	{
		// Only disappearing and moving objects have to be managed. 
		if (disableAfter || moving)
			InteractableObjectManager.instance.Register (this);
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject.tag == "Player") 
		{
			ApplyEffect (changeAmount, other.gameObject);

			if (disableAfter)
				gameObject.SetActive (false);
		}
	}

	public virtual void ApplyEffect (int amount, GameObject other) { }
}
