﻿using UnityEngine;
using System.Collections;

public class LifeChangerEffect : ChangeEffect 
{
	[SerializeField]
	bool deadly = false;

	public override void ApplyEffect (int amount, GameObject other)
	{
		PlayerLife otherLife = other.GetComponent<PlayerLife> ();

		if (deadly) 
		{
			otherLife.Kill ();
			return;
		} 
		else
			otherLife.AddLife (changeAmount);
	}
}
